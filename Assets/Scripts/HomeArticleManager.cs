using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeArticleManager : MonoBehaviour
{
  ArticleData _articleData;
  public Button diagnosaPage, konsultasiPage, ambulansPage;

  private void Start()
  {
    _articleData = FindObjectOfType<ArticleData>();

    diagnosaPage.onClick.AddListener(() =>
    {
      ScenePageManager.Instance.DiagnosaPage();
    });
    konsultasiPage.onClick.AddListener(() =>
    {
      ScenePageManager.Instance.KonsultasiPage();
    });
    ambulansPage.onClick.AddListener(() =>
    {
      ScenePageManager.Instance.AmbulansPage();
    });
  }

  public void ArticleData(int paramsID)
  {
    _articleData.SetArticleID(paramsID);
    ScenePageManager.Instance.ArticlePage();
  }
}
