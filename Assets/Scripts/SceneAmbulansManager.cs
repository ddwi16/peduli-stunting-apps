using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneAmbulansManager : MonoBehaviour
{
  public GameObject backBtn;

  private void Start()
  {
    backBtn.GetComponent<Button>().onClick.AddListener(() =>
    {
      ScenePageManager.Instance.HomePage();
    });
  }
}
