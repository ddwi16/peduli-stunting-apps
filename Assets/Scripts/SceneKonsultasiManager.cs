using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneKonsultasiManager : MonoBehaviour
{
  public GameObject backBtn;
  public GameObject messageObj;

  private void Start()
  {
    backBtn.GetComponent<Button>().onClick.AddListener(() =>
    {
      ScenePageManager.Instance.HomePage();
    });

    StartCoroutine(MessagePopUp());
  }

  IEnumerator MessagePopUp()
  {
    yield return new WaitForSeconds(1);
    messageObj.SetActive(true);
  }
}
