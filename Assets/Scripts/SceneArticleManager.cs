using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneArticleManager : MonoBehaviour
{
  ArticleData _articleData;

  public List<GameObject> articleObj = new List<GameObject>();
  public List<GameObject> backBtn;

  // Start is called before the first frame update
  void Awake()
  {
    _articleData = FindObjectOfType<ArticleData>();

    if (_articleData.ArticleID == 0)
    {
      articleObj[1].SetActive(false);
    }
    else
    {
      articleObj[1].SetActive(true);
    }

    foreach (GameObject back in backBtn)
    {
      back.GetComponent<Button>().onClick.AddListener(() =>
      {
        ScenePageManager.Instance.HomePage();
      });
    }
  }
}
