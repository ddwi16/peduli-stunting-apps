using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MessageManager : MonoBehaviour
{
  public TMP_InputField messageField;
  public Button sentBtn;
  public GameObject messagePrefab;
  public RectTransform messageContainer;

  void Start()
  {
    sentBtn.onClick.AddListener(() =>
    {
      var textMsg = messageField.text;

      var imageMessage = messagePrefab.transform.GetChild(0);
      var textMessage = imageMessage.transform.GetChild(0);

      textMessage.GetComponent<TMP_Text>().text = textMsg;

      Instantiate(messagePrefab, transform.position, Quaternion.identity, messageContainer);

      messageField.text = "";
    });
  }
}
