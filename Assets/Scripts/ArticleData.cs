using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArticleData : MonoBehaviour
{
  private int articleID = 0;

  public int ArticleID { get { return articleID; } }

  public void SetArticleID(int newID)
  {
    articleID = newID;
  }
}
