using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using Firebase.Auth;
using TMPro;

public class AuthManager : MonoBehaviour
{
  public DependencyStatus dependencyStatus;
  public FirebaseAuth firebaseAuth;
  public FirebaseUser firebaseUser;

  public TMP_InputField emailInputField;
  public TMP_InputField passwordInputField;
  public TMP_Text errorMessageField;

  public Button loginBtn;

  // Start is called before the first frame update
  void Awake()
  {
    FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
    {
      dependencyStatus = task.Result;
      if (dependencyStatus == DependencyStatus.Available)
      {
        InitializeFirebase();
      }
      else
      {
        Debug.LogError("Unable to initialize Firebase. " + dependencyStatus);
      }
    });

    loginBtn.onClick.AddListener(() =>
    {
      StartCoroutine(LoginTask(emailInputField.text, passwordInputField.text));
    });
  }

  private void InitializeFirebase()
  {
    firebaseAuth = FirebaseAuth.DefaultInstance;
  }

  private IEnumerator LoginTask(string _email, string _password)
  {
    //Call the Firebase auth signin function passing the email and password
    var LoginTask = firebaseAuth.SignInWithEmailAndPasswordAsync(_email, _password);
    //Wait until the task completes
    yield return new WaitUntil(predicate: () => LoginTask.IsCompleted);

    if (LoginTask.Exception != null)
    {
      //If there are errors handle them
      Debug.LogWarning(message: $"Failed to register task with {LoginTask.Exception}");
      FirebaseException firebaseEx = LoginTask.Exception.GetBaseException() as FirebaseException;
      AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

      string message = "Login Failed!";
      switch (errorCode)
      {
        case AuthError.MissingEmail:
          message = "Missing Email";
          break;
        case AuthError.MissingPassword:
          message = "Missing Password";
          break;
        case AuthError.WrongPassword:
          message = "Wrong Password";
          break;
        case AuthError.InvalidEmail:
          message = "Invalid Email";
          break;
        case AuthError.UserNotFound:
          message = "Account does not exist";
          break;
      }
      errorMessageField.text = message;
    }
    else
    {
      firebaseUser = LoginTask.Result;
      errorMessageField.text = "";
      ScenePageManager.Instance.HomePage();
    }
  }
}
