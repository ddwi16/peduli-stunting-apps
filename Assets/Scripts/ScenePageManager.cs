using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenePageManager : MonoBehaviour
{
  public static ScenePageManager Instance;

  void Awake()
  {
    if (Instance != null && Instance != this)
    {
      Destroy(gameObject);
    }
    else
    {
      Instance = this;
      DontDestroyOnLoad(gameObject);
    }
  }

  public void WelcomePage()
  {
    SceneManager.LoadScene("Welcome Scene");
  }

  public void LoginPage()
  {
    SceneManager.LoadScene("Login Scene");
  }

  public void HomePage()
  {
    SceneManager.LoadScene("Home Scene");
  }

  public void ArticlePage()
  {
    SceneManager.LoadScene("Article Scene");
  }

  public void DiagnosaPage()
  {
    SceneManager.LoadScene("Diagnosa Scene");
  }

  public void KonsultasiPage()
  {
    SceneManager.LoadScene("Konsultasi Scene");
  }

  public void AmbulansPage()
  {
    SceneManager.LoadScene("Ambulans Scene");
  }
}

